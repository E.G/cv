<?php

class ControllerMail
{
    public function __construct ()
    {

    }

    private static function sendMail ( $headerMessage , $mailDestination , $subject , $message )
    {
        if ( ! preg_match ( "#^[a-z0-9._-]+@(hotmail|live|msn|outlook).[a-z]{2,4}$#" , $mailDestination ) ) // On filtre les serveurs qui rencontrent des bogues.
        {
            $passage_ligne = "\r\n";
        } else {
            $passage_ligne = "\n";
        }
        //=====Création de la boundary
        $boundary = "-----=" . md5 ( rand () );
        //==========
        $header           = "From: \"Gardenier Eloy\" <contact@eloy-development.web-edu.fr>" . $passage_ligne;
        $header           .= "MIME-Version: 1.0" . $passage_ligne;
        $header           .= "Content-Type: multipart/alternative;" . $passage_ligne . " boundary=\"$boundary\"" . $passage_ligne;
        $containerContent = '
        <html lang="fr">
            <body>
                <div>
                    <br>

                    <div style="margin: auto; color: black;">
                        ' . $headerMessage . '
                    </div>

                    <br>

                    <div style="width: 60%; margin: auto; text-align: center; color: black; font-size: 1.12em;">
                        ' . $message . '
                    </div>

                </div>
            </body>
        </html>
    ';

        $formatedContent = $passage_ligne . "--" . $boundary . $passage_ligne;
        $formatedContent .= "Content-Type: text/html; charset=\"UTF-8\"" . $passage_ligne;
        $formatedContent .= "Content-Transfer-Encoding: 8bit" . $passage_ligne;
        $formatedContent .= $passage_ligne . $containerContent . $passage_ligne;
        $formatedContent .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;
        $formatedContent .= $passage_ligne . "--" . $boundary . "--" . $passage_ligne;

        $send = mail ( $mailDestination , $subject , $formatedContent , $header );
        if ( $send ) {
            return $send;
        } else {
            return false;
        }
    }

    public static function mailContact ( $mailUser , $mailDestination , $message , $name , $struct )
    {
        $subject = "Contact";

        if ( $struct !== null ) {
            $headerMessage = '
            <h3>Nom de l\'expéditeur : ' . $name . '</h3>
            <h3>Nom de la structure : ' . $struct . '</h3>
            <h3>Email de l\'expéditeur : ' . $mailUser . '</h3><br>
            <h3>Message : </h3>
            ';
        } else {
            $headerMessage = '
            <h3>Nom de l\'expéditeur : ' . $name . '</h3>
            <h3>Email de l\'expéditeur : ' . $mailUser . '</h3><br>
            <h3>Message : </h3>
            ';
        }

        $send = ControllerMail ::sendMail ( $headerMessage , $mailDestination , $subject , $message );
        if ( $send ) {
            return $send;
        } else {
            return false;
        }
    }

    public static function mailConfirmation ( $mailDestination , $message , $name , $struct )
    {
        $subject = 'Confirmation de réception';
        if ( $struct !== null ) {
            $headerMessage = '
                    <h3>Bonjour, ' . $name . ' de ' . $struct . '</h3>
                    <h3>Vous venez d\'envoyer un courriel a M. Eloy Gardenier</h3><br>
                    <h3>Récapitulatif de votre message : </h3>
                    ';
        } else {
            $headerMessage = '
                    <h3>Bonjour, ' . $name . '</h3>
                    <h3>Vous venez d\'envoyer un courriel a M. Eloy Gardenier</h3><br>
                    <h3>Récapitulatif de votre message : </h3>
                    ';
        }
        $send = ControllerMail ::sendMail ( $headerMessage , $mailDestination , $subject , $message );
        if ( $send ) {
            return $send;
        } else {
            return false;
        }
    }

    function isValid ( $value )
    {
        if ( filter_var ( $value , FILTER_VALIDATE_EMAIL ) && preg_match ( '/@.+\./' , $value ) ) {
            return strip_tags ( $value );
        } else {
            return 1;
        }
    }


    public function send ()
    {

        // On vérifie que le visiteur vient du formulaire
        if ( isset( $_SERVER[ 'HTTP_ORIGIN' ] ) && $_SERVER[ 'HTTP_ORIGIN' ] == "https://www.eloy-development.web-edu.fr" ) {
            if ( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) {

                // On vérifie que le champ "Pot de miel" est vide
                if ( isset( $_POST[ 'raison' ] ) && empty( $_POST[ 'raison' ] ) ) {

                    $error =  false;
                    $condition = !empty( $_POST[ 'condition' ] ) && isset( $_POST[ 'condition' ] ) ? strip_tags ($_POST[ 'condition' ]) : null;
                    $mailUser = !empty( $_POST[ 'email' ] ) && isset( $_POST[ 'email' ] ) ? ControllerMail ::isValid ( $_POST[ 'email' ] ) : null;
                    $message = !empty( $_POST[ 'message' ] ) && isset( $_POST[ 'message' ] ) ? htmlspecialchars ( $_POST[ 'message' ] ) : null;
                    $name = !empty( $_POST[ 'name' ] ) && isset( $_POST[ 'name' ] ) ? strip_tags ( $_POST[ 'name' ] ) : null;
                    $struct = !empty( $_POST[ 'enterprise' ] ) && isset( $_POST[ 'enterprise' ] ) ? strip_tags ( $_POST[ 'enterprise' ] ) : null;

                    switch ( $condition ) {
                        //Vérification que la chekbox à bien été cochée
                        case null :
                            $error = true;
                            break;
                    }

                    switch ( $mailUser ) {
                        //Message d'erreur si la valeur ne correspond pas au regex
                        case 1:
                            //Vérification que le email à bien été entré
                        case null :
                            $error = true;
                            break;
                    }

                    switch ( $message ) {
                        //Vérification que le message à bien été entré
                        case null :
                            $error = true;
                            break;
                    }

                    switch ( $name ) {
                        //Vérification que le nom à bien été entré
                        case null :
                            $error = true;
                            break;
                    }

                    if ( $error == true ) {
                        new Exception( 'mail' );
                        exit;
                    }

                        //Envoi du message de l'utilisateur
                        if ( ! ControllerMail ::mailContact ( $mailUser , 'contact@eloy-development.web-edu.fr' , $message , $name , $struct ) ) {
                            $error = true;
                        }

                        //Envoi du message de confirmation
                        if ( ! ControllerMail ::mailConfirmation ( $mailUser , $message , $name , $struct ) ) {
                            $error = true;
                        }

                        //Message d'erreur si un des envois c'est mal passé
                        if ( $error == true ) {
                            new Exception( 'mail' );
                        } else {
                            header ( 'Location: index.php?controller=views' );
                        }

                }
            } else {
                //Message d'erreur si le pot de miel n'est pas vide
                http_response_code ( 405 );
                new Exception( 'mail' );
                exit;
            }
        }
    }
}