<?php

class DisplayViews
{
  public function __construct()
  {

  }

  public function views(){
    ob_start();
    require('views/OnePage/intro.php');
    require('views/OnePage/portfolio.php');
    require('views/OnePage/contact.php');
    $content = ob_get_clean();
    require('views/_commun/template.php');
  }
}
