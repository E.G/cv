<?php
require_once('controller/ControllerViews.php');
require_once('controller/ControllerMail.php');
class Router
{

    public function display(){
        try
        {
            $request = array_merge($_GET, $_POST);
            if(isset($request['controller']))
            {
                $dv = new DisplayViews();
                $cm = new ControllerMail();

                if($request['controller'] == 'views')
                {
                    $dv->views();
                   
                }
                else if ($request['controller'] == 'mail') {
                    $sendMail = $cm->send();
                    if ($sendMail) {
                        header('Location: index.php?controller=views');
                    }else {
                        throw new Exception('mail');
                    }
                }
                else
                {
                    throw new Exception('views');
                }
            }
            else
            {
                header('Location: index.php?controller=views');
            }
        }

        catch(Exception $e)
        {
            $errorMessage = $e->getMessage();
            if ($errorMessage == 'mail') {
                require_once('views/errorMail.php');
            }else if ($errorMessage == 'views'){
                require_once('views/errorView.php');
            }else {
                require_once('views/errorView.php');
            }
        }
    }
}