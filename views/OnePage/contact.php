				<!-- Contact -->
                <section id="contact" class="four">
						<div class="container">

							<header>
								<h2>Travaillons ensemble</h2>
							</header>

							<p>Je suis prêt à étudier toute offre intéressante.</p>

							<form method="POST" action="index.php?controller=mail" class='contact-form'>
								<div class="row row-contact">
									<div class="row inline">
                                        <input type="hidden" name="raison">
										<div class="col-6 col-6-mobile"><input type="text" name="name" placeholder="Nom *" required /></div>
										<div class="col-6 col-6-mobile"><input type="text" name="enterprise" placeholder="Structure"/></div>
									</div>
									<div class="col-6 col-12-mobile"><input type="email" name="email" placeholder="Email *" required /></div>
									<div class="col-12">
										<textarea name="message" placeholder="Message *" required ></textarea>
									</div>
									<div class="condition"><input type="checkbox" name="condition" id="condition"  value="condition">
									<label class="label-condition" for="condition">En soumettant ce formulaire, j’accepte que les informations saisies dans ce formulaire soient utilisées, exploitées, traitées pour seulement permettre de me recontacter.</label></div>
									<div class="col-12">
										<input type="submit" id="submit-mail" value="Envoyer le message" disabled/>
									</div>
								</div>
							</form>
						</div>
					</section>
