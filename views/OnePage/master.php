        <div class="bar">
          <ul>
            <li class="masteries">HTML<br>
              <progress value="95" max="100">95 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">CSS<br>
              <progress value="80" max="100">80 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">JavaScript<br>
              <progress value="80" max="100">80 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">PHP<br>
              <progress value="70" max="100">70 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">Node.js
              <progress value="70" max="100">70 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">Meteor.js
              <progress value="80" max="100">80 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
            <li class="masteries">Symfony
              <progress value="50" max="100">50 %</progress>
            </li>
            <li class="font-min masteries">J'utilise régulierement ce language et en suis passionné.</li>
          </ul>
        </div>