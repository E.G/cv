				<!-- Portfolio -->
					<section id="portfolio" class="two">
						<div class="container">

							<header>
								<h2>Portfolio</h2>
							</header>

							<p>
                                Voici mes dernières réalisations !
                                <br>
                                Cliquez dessus pour les découvrir.
                            </p>

							<div class="row group-item">
									<article class="item">
										<a target="_blank" href="https://www.pole-image.yaya-projects.web-edu.fr" class="image fit"><div style="background: url('public/img/pole-image.png')"> </div>
										<header>
											<h3>POLE-IMAGE</h3>
										</header>
										</a>
									</article>
									<article class="item">
										<a target="_blank" href="https://www.yaya-projects.web-edu.fr" class="image fit"><div style="background: url('public/img/eep.png')"> </div>
										<header>
											<h3>EEP SIMPLON</h3>
										</header>
										</a>
									</article>
									<article class="item">
										<a target="_blank" href="https://e-mouvant.com" class="image fit"><div style="background: url('public/img/e-mouvant.png')"></div>
										<header>
											<h3>e-mouvant</h3>
										</header>
										</a>
									</article>
                                    <article class="item">
                                        <a target="_blank" href="https://cleanbill.fr" class="image fit"><div style="background: url('public/img/cleanbill.png')"></div>
                                            <header>
                                                <h3>Clean Bill</h3>
                                            </header>
                                        </a>
                                    </article>
									<!--<article class="item">
                                        <a target="_blank" href="https://fixetonrdv.com" class="image fit"><div style="background: url('public/img/cleanbill.png')"></div>
                                            <header>
                                                <h3>Clean Bill</h3>
                                            </header>
                                        </a>
                                    </article>-->
							</div>

						</div>
					</section>