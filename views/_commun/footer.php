		<!-- Footer -->
        <div id="footer">

<!-- Copyright -->
    <ul class="copyright">
        <li>&copy; Gardenier Eloy. All rights reserved.</li>
    </ul>

</div>

<!-- Scripts -->
<script src="public/js/jquery.min.js"></script>
<script src="public/js/jquery.scrolly.min.js"></script>
<script src="public/js/jquery.scrollex.min.js"></script>
<script src="public/js/browser.min.js"></script>
<script src="public/js/breakpoints.min.js"></script>
<script src="public/js/util.js"></script>
<script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
<script src="public/js/main.js" async></script>

