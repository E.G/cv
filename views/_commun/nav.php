<!-- Nav -->
<nav id="nav">
	<ul>
		<li><a href="#top" class="nav-link" id="top-link"><span class="fas fa-home"><h6 class="text-nav">Intro</h6></span></a></li>
		<!--<li><a href="#about" class="nav-link" id="about-link"><span class="fas fa-id-card"><h6 class="text-nav">À propos</h6></span></a></li>-->
		<li><a href="#portfolio" class="nav-link" id="portfolio-link"><span class="fas fa-th"><h6 class="text-nav">Portfolio</h6></span></a></li>
		<li><a href="#contact" class="nav-link" id="contact-link"><span class="fas fa-paper-plane"><h6 class="text-nav">Contact</h6></span></a></li>
	</ul>
</nav>