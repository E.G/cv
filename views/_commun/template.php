<!DOCTYPE HTML>
<html lang="fr_FR">
<head>
    <title>Réalisations de Gardenier Eloy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <!-- Primary Meta Tags -->
    <meta name="title" content="Réalisations de Gardenier Eloy">
    <meta name="description" content="Ceci est mon portfolio, vous pourrez découvrir ici les différentes créations que j'ai déjà pu effectuer.">
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://eloy-development.web-edu.fr">
    <meta property="og:title" content="Réalisations de Gardenier Eloy">
    <meta property="og:description" content="Ceci est mon portfolio, vous pourrez découvrir ici les différentes créations que j'ai déjà pu effectuer.">
    <meta property="og:image" content="https://eloy-development.web-edu.fr/public/css/images/gif.gif">
    <meta property="og:image:alt" content="A landscape" />
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://eloy-development.web-edu.fr">
    <meta property="twitter:title" content="Réalisations de Gardenier Eloy">
    <meta property="twitter:description" content="Ceci est mon portfolio, vous pourrez découvrir ici les différentes créations que j'ai déjà pu effectuer.">
    <meta property="twitter:image" content="https://eloy-development.web-edu.fr/public/css/images/gif.gif">
    <meta property="twitter:image:alt" content="A landscape" />

    <link rel="stylesheet" href="public/css/main.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="icon" href="public/img/logo.png" type="image/png"/>
</head>
<body class="is-preload">

<!-- Header -->
<header id="header">

    <div class="top">

        <!-- Logo -->
        <div id="logo">
        <div class="badge-base LI-profile-badge" data-locale="fr_FR" data-size="medium" data-theme="light" data-type="VERTICAL" data-vanity="yol-r" data-version="v1"></div>
            <p class="blue underline">contact<span class="anti-bot">@elo-project8ze236541elo.web-edu.frfgq@sqdaefsq.web-edu.fr</span>@eloy-development<span class="anti-bot">@eloyjkfzze8236sdfsd.fqz@541sqd</span>.web-edu.<span class="anti-bot">com82juykuicomluil36541com</span>fr</p>
            <p class="blue underline">06 <span class="anti-bot">82364524541</span>08 <span class="anti-bot">82364524541</span>67 <span class="anti-bot">823566541</span>03 <span class="anti-bot">82354006541</span>62</p>
        </div>
        <?php
        include 'nav.php';
        ?>
    </div>

    <div class="bottom">

        <!-- Social Icons -->
        <ul class="icons">
            <li><a href="https://gitlab.com/E.G" target="_blank" class="fab fa-gitlab"></a></li>
            <li><a href="https://github.com/IdoSoIam" target="_blank" class="fab fa-github"></a></li>
            <li><a href="https://discord.gg/KpJYJUt" target="_blank" class="fab fa-discord"></a></li>
        </ul>

    </div>
</header>
<div id="main">
    <?php
    echo $content;
    include 'footer.php'
    ?>
</div>
</body>
</html>
